export const storefrontRequest = async (query) => {
  const response = await fetch("https://gatsby-sandbox-convert.myshopify.com/api/2022-01/graphql",
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/graphql",
        "X-Shopify-Storefront-Access-Token": "d97a0b612272fa30ac7ca56315d020f0"
      },
      body: query
    }
  )
  return await response.json();
}