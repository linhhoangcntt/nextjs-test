export const getCart = async () => {
  const cartResponse = await fetch('/cart.json', {
    headers: {
      'Content-Type': 'application/json'
    },
  });
  return await cartResponse.json();
}