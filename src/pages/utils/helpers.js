export const formatMoney = (price) => {
  return Intl.NumberFormat("en-AU", { style: 'currency', currency: 'AUD', minimumFractionDigits: 0}).format(price);
}