import Head from "next/head";
import { useState, useEffect } from "react"
import { storefrontRequest } from "./api/storefront"
import { getCart } from "./api/cart"
import { formatMoney } from "./utils/helpers"

const productQuery = `query Products {
  products (first: 1) {
    edges {
      node {
        title
        description
        priceRange {
          minVariantPrice {
            amount
          }
        }
        images(first: 1) {
          edges {
            node {
              src
              altText
            }
          }
        }
      }
    }
  }
}`

export const getStaticProps = async() => {
  const { data } = await storefrontRequest(productQuery);
  
  return {
    props: {
      product: data ? data.products.edges[0].node : null
    }
  }
}

export default function Home({ product }) {
  const [addingToCart, setAddingToCart] = useState(false);
  const [cart, setCart] = useState(null);
  const image = product.images.edges[0].node;

  const addToCart = async() => {
    setAddingToCart(!addingToCart);
  }

  // Reload cart state whenever cart changes
  useEffect(() => {
    (async () => {
      const _cart = await getCart();
      setCart(_cart);
    })();
  }, [cart]);  

  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="flex flex-col items-center justify-center w-full flex-1 px-5 lg:px-20 text-center">
        <div className="w-full max-w-screen-xl lg:grid lg:grid-cols-2 lg:gap-x-8">
          <div className="bg-gray-100 overflow-hidden mb-8 lg:mb-0">
            <img src={ image.src } alt={ image.altText } />
          </div>
          <div className="text-left max-w-sm">
            <h1 className="text-2xl mb-1 font-light">{ product.title }</h1>
            <div className="mb-2 lg:mb-4 text-black">
              <span className="inline-block align-middle text-base font-bold">{ formatMoney(product.priceRange.minVariantPrice.amount) }</span>
            </div>
            <div className="mb-8">
              { product.description }
            </div>
            <button
              type="button"
              role="button"
              name="addToCart"
              className="bg-black text-white border border-black text-center p-3 font-bold uppercase hover:bg-white hover:text-black flex items-center justify-center w-full h-5-6 no-underline"
              aria-label="Add To Cart"
              onClick={ addToCart }
            >
              { addingToCart && (<span>Adding To Cart...</span>) }
              <span className={`${addingToCart ? 'hidden': null}`}>Add To Cart</span>
            </button>
          </div>
          
        </div>
      </main>

      <footer className="flex items-center justify-center w-full h-24 border-t">
        footer
      </footer>
    </div>
  );
}
